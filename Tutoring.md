---
layout: post
title: "Tutoring"
comments: true
categories: misc
---

I provide free tutoring in physics. 

## Subjects Offered
I tutor at the high school, AP, and Olympiad levels.

Specifically - AP Physics 1, AP Physics 2, AP Physics C: Mechanics, AP Physics C: Electricity and Magnetism, and the Physics Olympiad.

## Sign Up
If you would like to receive tutoring, please fill out this [Google form](https://forms.gle/MS47k5ck8Eykjwk97).
