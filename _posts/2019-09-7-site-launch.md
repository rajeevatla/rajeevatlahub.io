---
layout: post
title: "Site Launch"
comments: true
categories: misc
---

My site finally launched!!!

After a long night of work, I was finally able to launch my site! I would like to thank GitHub Pages so much for letting me do this! The alternative was hosting with AWS, and that's expensive, so GitHub Pages FTW! (▀̿Ĺ̯▀̿ ̿)
